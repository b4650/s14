let fname = "John";
let lname = "Smith";
let age = 30;
let hobby1 = "Biking";
let hobby2 = "Mountain Climbing";
let hobby3 = "Swimming";
let houseNumber = 32;
let street = "Washington";
let city = "Lincoln";
let state = "Nebraska";

/*console.log("Hello World")*/ 
console.log("First Name: " + fname); 
console.log("Last Name: " + lname); 
console.log("Age: " + age); 
console.log("Hobbies:"); 
console.log(`(3) ["${hobby1}", "${hobby2}", "${hobby3}]"`); 
console.log("Work Address:");
console.log(`${houseNumber}, ${street}, ${city}, ${state}`);

function printUserInfo(fname, lname, age, hobby1, hobby2, hobby3,houseNumber, street, city, state){
	console.log(`${fname} ${lname} is ${age} years of age.`)
	console.log("This was printed inside of the function")
	console.log(`${hobby1}, ${hobby2}, ${hobby3}`)
	console.log("This was printed inside of the function")
	console.log(`${houseNumber}, ${street}, ${city}, ${state}`);
} 

printUserInfo("John", "Smith", 30, "Biking", "Mountain Climbing", "Swimming", "32", "Washington", "Lincoln", "Nebraska");

function returnFunction(isMarried){
	return `The value of isMarried: ${isMarried}`;
}
let mStatus = returnFunction(true)
console.log(mStatus)